package com.aranirahan.rotiku.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aranirahan.rotiku.R;
import com.aranirahan.rotiku.activity.MainActivity;
import com.aranirahan.rotiku.adapter.BookingAdapter;
import com.aranirahan.rotiku.model.SubTransaksiItem;
import com.aranirahan.rotiku.model.TransaksiItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TabFragmentBooking extends Fragment {

    private DatabaseReference databaseReference;
    private RecyclerView rvBookingRoti;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    public TabFragmentBooking() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tab_fragment_booking, container, false);
        final FragmentActivity c = getActivity();
        rvBookingRoti = view.findViewById(R.id.rv_booking);

        databaseReference = MainActivity.getDatabase().getReference();

        showRecyclerList();

        return view;
    }

    private void showRecyclerList(){
        databaseReference.child("transaksi").orderByChild("status").equalTo("booking").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                final ArrayList<TransaksiItem> listBookingTransaksi = new ArrayList<>();


                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    final String itemKey = noteDataSnapshot.getKey();
                    final String nama_pesan = noteDataSnapshot.child("nama_pesan").getValue().toString();
                    final String status = noteDataSnapshot.child("status").getValue().toString();
                    final String tgl_ambil = noteDataSnapshot.child("tgl_ambil").getValue().toString();
                    final String tgl_transaksi = noteDataSnapshot.child("tgl_transaksi").getValue().toString();
                    final String total = noteDataSnapshot.child("total").getValue().toString();

                    DatabaseReference subKey = databaseReference.child("sub_produk");
                    subKey.child(itemKey).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataS) {
                            final ArrayList<SubTransaksiItem> subTransaksiItems = new ArrayList<>();
                            for (DataSnapshot snapshot : dataS.getChildren()) {

                                final String produk = snapshot.child("produk").getValue().toString();
                                final String qty = snapshot.child("qty").getValue().toString();
                                final String total = snapshot.child("total").getValue().toString();

                                subTransaksiItems.add(new SubTransaksiItem(produk, qty, total));
                            }

                            listBookingTransaksi.add(new TransaksiItem(itemKey, nama_pesan, status,
                                    tgl_ambil, tgl_transaksi, total, subTransaksiItems));

                            BookingAdapter listBookingAdapter = new BookingAdapter(listBookingTransaksi, getContext());
                            RecyclerView.ItemAnimator animator = rvBookingRoti.getItemAnimator();
                            if (animator instanceof DefaultItemAnimator) {
                                ((DefaultItemAnimator) animator).setSupportsChangeAnimations(false);
                            }
                            rvBookingRoti.setLayoutManager(new LinearLayoutManager(getContext()));
                            rvBookingRoti.setAdapter(listBookingAdapter);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Log.w("onCancelled", databaseError.toException());
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

