package com.aranirahan.rotiku.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aranirahan.rotiku.R;
import com.aranirahan.rotiku.adapter.HistoryAdapter;

public class HistoryFragment extends Fragment {

    Toolbar toolbar;
    TabLayout tabLayout;

    
    public HistoryFragment() {
        // Required empty public constructor
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_history, container, false);
        final FragmentActivity c = getActivity();

        //setSupportActionBar(toolbar);
        tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Booking"));
        tabLayout.addTab(tabLayout.newTab().setText("Selesai"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = view.findViewById(R.id.pager);
        final HistoryAdapter adapter = new HistoryAdapter(c.getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return view;
    }


//    public void setSupportActionBar(Toolbar supportActionBar) {
//        this.supportActionBar = supportActionBar;
//    }
}

//  //Cara lain menggunakan thread
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                rvDashboardRoti.setLayoutManager(new GridLayoutManager(getContext(), 4));
//                final DashboardAdapter gridRotiAdapter = new DashboardAdapter(c);
//                c.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        gridRotiAdapter.setRotiItems(listDashboardRoti);
//                        rvDashboardRoti.setAdapter(gridRotiAdapter);
//                    }
//                });
//            }
//        }).start();
//
//        return view;
//
//    }
//
//}

