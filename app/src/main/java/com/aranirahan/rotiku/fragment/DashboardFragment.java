package com.aranirahan.rotiku.fragment;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aranirahan.rotiku.R;
import com.aranirahan.rotiku.activity.MainActivity;
import com.aranirahan.rotiku.adapter.DashboardAdapter;
import com.aranirahan.rotiku.helper.DataTransferInterface;
import com.aranirahan.rotiku.model.PushTransaksi;
import com.aranirahan.rotiku.model.RotiItem;
import com.aranirahan.rotiku.model.SubTransaksiItem;
import com.aranirahan.rotiku.model.TransaksiItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class DashboardFragment extends Fragment {

    private DatabaseReference databaseReference;
    private RecyclerView rvDashboardRoti;
    private RecyclerView rvKeranjangRoti;
    private ArrayList<RotiItem> listDashboardRoti = new ArrayList<>();
    private ArrayList<SubTransaksiItem> subTransaksiItems = new ArrayList<>();
    private ArrayList<TransaksiItem> transaksiItems = new ArrayList<>();
    private Button btnBooking, btnBayar;
    private EditText edtNama, edtTglAmbil, edtBayar, edtKembalian, edtContact, edtAlamat, edtCatatan;
    private EditText db_edtKembalian, db_edtBayar, db_edtKeterangan;
    private TextInputLayout layTglAmbil;
    private TextView txtTotal;

    AlertDialog d, db;
    int kembalian, totalBayar = 0;
    Calendar calendar = Calendar.getInstance();
    String myFormat = "dd/MM/yy";
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
    java.util.TimeZone tz = java.util.TimeZone.getTimeZone("UTC+7");
    String key;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        final View dialogBooking = inflater.inflate(R.layout.dialog_booking, null);
        final View dialogBayar = inflater.inflate(R.layout.dialog_bayar, null);
        final FragmentActivity c = getActivity();

        //ambil id setiap inputan
        rvDashboardRoti = view.findViewById(R.id.rv_dashboard_roti);
        rvKeranjangRoti = view.findViewById(R.id.rv_keranjang);
        btnBayar = view.findViewById(R.id.btnBayar);
        btnBooking = view.findViewById(R.id.btnBooking);
        txtTotal = view.findViewById(R.id.keranjang_total);

        edtNama = dialogBooking.findViewById(R.id.nama_pemesan);
        edtTglAmbil = dialogBooking.findViewById(R.id.tgl_ambil);
        edtBayar = dialogBooking.findViewById(R.id.edtBayar);
        edtKembalian = dialogBooking.findViewById(R.id.edtKembalian);
        edtAlamat = dialogBooking.findViewById(R.id.edtAlamat);
        edtCatatan = dialogBooking.findViewById(R.id.edtCatatan);
        edtContact = dialogBooking.findViewById(R.id.edtContact);

        db_edtBayar = dialogBayar.findViewById(R.id.db_edtBayar);
        db_edtKembalian = dialogBayar.findViewById(R.id.db_edtKembalian);
        db_edtKeterangan = dialogBayar.findViewById(R.id.db_edtKeterangan);

        //setting definisi firebase

        databaseReference = MainActivity.getDatabase().getReference();
        databaseReference.keepSynced(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(c);

        //setup dialog booking
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        dialogBuilder.setView(dialogBooking);
        dialogBuilder.setTitle("Pesanan Booking");
        dialogBuilder.setMessage("silahkan isi sesuai pesanan");
        dialogBuilder.setPositiveButton("Booking", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Calendar cal = Calendar.getInstance(tz);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm:ss", Locale.ENGLISH);

                PushTransaksi send = new PushTransaksi(edtAlamat.getText().toString(), edtBayar.getText().toString(),
                        edtCatatan.getText().toString(), edtContact.getText().toString(), "syidik",
                        edtNama.getText().toString(), "booking", edtTglAmbil.getText().toString(),
                        sdf.format(cal.getTime()), String.valueOf(totalBayar));

                key = databaseReference.child("transaksi").push().getKey();

                databaseReference.child("transaksi/"+key).setValue(send);
                databaseReference.child("sub_produk/"+key).setValue(subTransaksiItems);
                Toast.makeText(c, "Berhasil ditambahkan "+totalBayar, Toast.LENGTH_SHORT).show();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        d = dialogBuilder.create();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        edtTglAmbil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getContext(), date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        edtBayar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() != 0) {
                    kembalian = Integer.parseInt(s.toString()) - totalBayar;

                    edtKembalian.setText(String.valueOf(kembalian));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtKembalian.setEnabled(false);
        //end setting dialog

        //setup dialog bayar
        final AlertDialog.Builder dialogBuilderBayar = new AlertDialog.Builder(getContext());
        dialogBuilderBayar.setView(dialogBayar);
        dialogBuilderBayar.setTitle("Pesan cash");
        dialogBuilderBayar.setMessage("silahkan isi sesuai pesanan");
        dialogBuilderBayar.setPositiveButton("Bayar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            //transaksiItems.add(new TransaksiItem("syidik", ));
            Calendar cal = Calendar.getInstance(tz);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm:ss", Locale.ENGLISH);

            PushTransaksi send = new PushTransaksi("-", db_edtBayar.getText().toString(),
                    db_edtKeterangan.getText().toString(), "-", "syidik",
                    "guest", "selesai", "-",
                    sdf.format(cal.getTime()), String.valueOf(totalBayar));

            key = databaseReference.child("transaksi").push().getKey();

            databaseReference.child("transaksi/"+key).setValue(send);
            databaseReference.child("sub_produk/"+key).setValue(subTransaksiItems);
            Toast.makeText(c, "Berhasil ditambahkan "+totalBayar, Toast.LENGTH_SHORT).show();
            }
        });
        dialogBuilderBayar.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        db_edtBayar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() != 0) {
                    kembalian = Integer.parseInt(s.toString()) - totalBayar;

                    db_edtKembalian.setText(String.valueOf(kembalian));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        db_edtKembalian.setEnabled(false);
        db = dialogBuilderBayar.create();
        //end setting dialog bayar

        //menampilkan produk
        databaseReference.child("products").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    RotiItem item = noteDataSnapshot.getValue(RotiItem.class);
                    item.setKey(noteDataSnapshot.getKey());

                    listDashboardRoti.add(item);
                }
                rvDashboardRoti.setLayoutManager(new GridLayoutManager(getContext(), 5));
                DashboardAdapter gridRotiAdapter = new DashboardAdapter(getContext(), communication);
                gridRotiAdapter.setRotiItems(listDashboardRoti, view);
                rvDashboardRoti.setAdapter(gridRotiAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //event klik btnBooking dengan pop up
        btnBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                totalBayar = 0;
                totalBayar = hitung();
                d.show();
                d.getWindow().setLayout(1200, 900);
            }
        });

        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                totalBayar = 0;
                totalBayar = hitung();
                db.show();
            }
        });
        return view;
    }

    DataTransferInterface communication = new DataTransferInterface() {
        @Override
        public void transfer(ArrayList<SubTransaksiItem> subs) {
            subTransaksiItems = subs;
            totalBayar = 0;
            totalBayar = hitung();
            txtTotal.setText(String.valueOf(totalBayar));
        }
    };

    private void updateLabel() {
        edtTglAmbil.setText(sdf.format(calendar.getTime()));
    }

    private int hitung() {
        //hitung total bayar
        if (subTransaksiItems.size() > 0) {
            for (int i=0; i < subTransaksiItems.size(); i++) {
                totalBayar = Integer.parseInt(subTransaksiItems.get(i).getTotal()) + totalBayar;
            }
        }
        return totalBayar;
        //end hitung
    }

    public void setTotal(int total) {
//        final View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
//        TextView txtTotal = view.findViewById(R.id.keranjang_total);
//        Log.d("totalBayar", txtTotal.getText().toString());
        //Toast.makeText(getActivity(), "hasil "+total, Toast.LENGTH_SHORT).show();
        //txtTotal.setText(String.valueOf(total));
    }
}

