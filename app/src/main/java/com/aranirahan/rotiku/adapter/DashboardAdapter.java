package com.aranirahan.rotiku.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aranirahan.rotiku.R;
import com.aranirahan.rotiku.helper.DataTransferInterface;
import com.aranirahan.rotiku.model.KeranjangItem;
import com.aranirahan.rotiku.model.RotiItem;
import com.aranirahan.rotiku.model.SubTransaksiItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.GridViewHolder> {
    private Context context;
    public ArrayList<RotiItem> rotiItems = new ArrayList<>();
    private ArrayList<KeranjangItem> listKeranjangRoti = new ArrayList<>();
    private ArrayList<SubTransaksiItem> subTransaksiItems = new ArrayList<>();
    RecyclerView rvKeranjangRoti;
    View view;
    int key = 0;
    private boolean cek = false;
    private DataTransferInterface mCommunicator;

    private ArrayList<RotiItem> getRotiItems() {
        return rotiItems;
    }

    public void setRotiItems(ArrayList<RotiItem> rotiItems, View v) {
        this.rotiItems = rotiItems;
        this.view = v;
    }

    public DashboardAdapter(Context context, DataTransferInterface commnunication) {
        this.context = context;
        this.mCommunicator = commnunication;
    }

    @Override
    public GridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_roti_dashboard, parent, false);
        return new GridViewHolder(view, mCommunicator);
    }

    @Override
    public void onBindViewHolder(final GridViewHolder holder, final int position) {
        Glide.with(context)
                .load(getRotiItems().get(position).getGambar())
                .apply(new RequestOptions().placeholder(R.drawable.ic_image_black_24dp).override(350, 550))
                .into(holder.imgRoti);
        holder.tvNama.setText(getRotiItems().get(position).getNama());

        final KeranjangItem keranjangItem = new KeranjangItem();
        final SubTransaksiItem sub = new SubTransaksiItem();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set keranjang item
                keranjangItem.setKey(getRotiItems().get(position).getKey());
                keranjangItem.setNama(getRotiItems().get(position).getNama());
                keranjangItem.setGambar(getRotiItems().get(position).getGambar());

                //set subTransaksi item
                sub.setProduk(getRotiItems().get(position).getNama());

                for (int i=0; i < listKeranjangRoti.size(); i++) {
                    if (listKeranjangRoti.get(i).getKey() == keranjangItem.getKey()) {
                        cek = true;
                        key = i;
                        Log.d("sama", listKeranjangRoti.get(i).getKey());
                        break;
                    }
                }
                Log.d("boo", String.valueOf(rotiItems.size()));
                if (cek) {
                    int qty = Integer.parseInt(listKeranjangRoti.get(key).getQty()) + 1;
                    int total = Integer.parseInt(listKeranjangRoti.get(key).getHarga()) * qty;
                    //set keranjang
                    keranjangItem.setQty(String.valueOf(qty));
                    keranjangItem.setTotal(String.valueOf(total));
                    listKeranjangRoti.set(key, keranjangItem);

                    //set subTransaksi
                    sub.setQty(String.valueOf(qty));
                    sub.setTotal(String.valueOf(total));
                    subTransaksiItems.set(key, sub);

                    cek = false;
                } else {
                    //set keranjang
                    keranjangItem.setHarga(getRotiItems().get(position).getHarga());
                    keranjangItem.setTotal(getRotiItems().get(position).getHarga());
                    keranjangItem.setQty("1");
                    listKeranjangRoti.add(keranjangItem);

                    //set subTransaksi
                    sub.setQty("1");
                    sub.setTotal(getRotiItems().get(position).getHarga());
                    subTransaksiItems.add(sub);
                    cek = false;
                }

                holder.mCommunication.transfer(subTransaksiItems);
                Log.d("trans", String.valueOf(subTransaksiItems.size()));

                rvKeranjangRoti.setLayoutManager(new LinearLayoutManager(context));
                KeranjangAdapter listRotiAdapter = new KeranjangAdapter(context, holder.mCommunication);
                listRotiAdapter.setRotiItems(listKeranjangRoti, subTransaksiItems);
                rvKeranjangRoti.setAdapter(listRotiAdapter);
            }


        });
    }

    @Override
    public int getItemCount() {
        return getRotiItems().size();
    }

    class GridViewHolder extends RecyclerView.ViewHolder {
        ImageView imgRoti;
        TextView tvNama;
        DataTransferInterface mCommunication;

        GridViewHolder(View itemView, DataTransferInterface Communicator) {
            super(itemView);
            rvKeranjangRoti = view.findViewById(R.id.rv_keranjang);
            imgRoti = itemView.findViewById(R.id.img_item_roti);
            tvNama = itemView.findViewById(R.id.tv_item_name);
            mCommunication = Communicator;
        }
    }
}