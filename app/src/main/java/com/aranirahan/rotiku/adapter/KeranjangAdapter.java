package com.aranirahan.rotiku.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aranirahan.rotiku.R;
import com.aranirahan.rotiku.helper.DataTransferInterface;
import com.aranirahan.rotiku.model.KeranjangItem;
import com.aranirahan.rotiku.model.SubTransaksiItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class KeranjangAdapter extends RecyclerView.Adapter<KeranjangAdapter.GridViewHolder> {
    private Context context;
    private ArrayList<KeranjangItem> rotiItems = new ArrayList<>();
    private ArrayList<SubTransaksiItem> subTransaksiItems = new ArrayList<>();
    private DataTransferInterface mCommunicator;
    int totalBayar = 0;

    public ArrayList<KeranjangItem> getRotiItems() {
        return rotiItems;
    }

    public void setRotiItems(ArrayList<KeranjangItem> rotiItems, ArrayList<SubTransaksiItem> transaksiItems) {
        this.rotiItems = rotiItems;
        this.subTransaksiItems = transaksiItems;
    }

    public KeranjangAdapter(Context context, DataTransferInterface communication) {
        this.context = context;
        this.mCommunicator = communication;
    }

    @Override
    public GridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_roti_keranjang, parent, false);

        return new GridViewHolder(view, mCommunicator);
    }

    @Override
    public void onBindViewHolder(final GridViewHolder holder, final int position) {
      Glide.with(context)
                .load(getRotiItems().get(position).getGambar())
//                .override(350, 550)
                .apply(new RequestOptions().placeholder(R.drawable.ic_image_black_24dp).override(60, 60))
                .into(holder.imgRoti);
        holder.tvNama.setText(getRotiItems().get(position).getNama());
        holder.tvHarga.setText(getRotiItems().get(position).getTotal());
        holder.etQty.setText(getRotiItems().get(position).getQty());

        Log.d("total"+position, String.valueOf(subTransaksiItems.get(position).getTotal()));
        //holder.mCommunication.transfer(subTransaksiItems);
        holder.etQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (holder.etQty.getText().toString().trim().length() != 0) {
                    int harga = Integer.parseInt(getRotiItems().get(position).getHarga()) * Integer.parseInt(holder.etQty.getText().toString());

                    rotiItems.get(position).setQty(holder.etQty.getText().toString());
                    rotiItems.get(position).setTotal(String.valueOf(harga));

                    subTransaksiItems.get(position).setQty(holder.etQty.getText().toString());
                    subTransaksiItems.get(position).setTotal(String.valueOf(harga));

                    Log.d("edit", String.valueOf(hitung()));
                    holder.mCommunication.transfer(subTransaksiItems);
                    holder.tvHarga.setText(getRotiItems().get(position).getTotal());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return getRotiItems().size();
    }

    public class GridViewHolder extends RecyclerView.ViewHolder {
        ImageView imgRoti;
        TextView tvNama;
        TextView tvHarga;
        EditText etQty;
        Button btnHapus;
        DataTransferInterface mCommunication;

        GridViewHolder(View itemView, DataTransferInterface Communicator) {
            super(itemView);

            imgRoti = itemView.findViewById(R.id.img_item_gambar_keranjang);
            tvNama = itemView.findViewById(R.id.tv_item_nama_keranjang);
            tvHarga = itemView.findViewById(R.id.tv_item_harga_keranjang);
            etQty = itemView.findViewById(R.id.edtQty);
            btnHapus = itemView.findViewById(R.id.bt_hapus);
            mCommunication = Communicator;

            btnHapus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rotiItems.remove(getAdapterPosition());
                    subTransaksiItems.remove(getAdapterPosition());
                    Log.d("hapus", String.valueOf(hitung()));
                    mCommunication.transfer(subTransaksiItems);
                    notifyDataSetChanged();
                }
            });
        }
    }

    private int hitung() {
        totalBayar = 0;
        //hitung total bayar
        if (subTransaksiItems.size() > 0) {
            for (int i=0; i < subTransaksiItems.size(); i++) {
                totalBayar = Integer.parseInt(subTransaksiItems.get(i).getTotal()) + totalBayar;
            }
        }
        return totalBayar;
        //end hitung
    }
}