package com.aranirahan.rotiku.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aranirahan.rotiku.R;
import com.aranirahan.rotiku.activity.MainActivity;
import com.aranirahan.rotiku.model.PushTransaksi;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

public class SubTitleViewHolder extends ChildViewHolder {

    public TextView txtNama, txtQty, txtTotal;
    public LinearLayout llBawah;
    public Button btnCicil, btnCetak;
    DatabaseReference databaseReference;
    String key, getTotal;
    EditText db_edtBayar, db_edtKembalian, db_edtKeterangan;
    AlertDialog db;
    int kembalian, totalBayar = 0;
    PushTransaksi transaksi;

    public SubTitleViewHolder(View itemView, Context ctx, ViewGroup parent) {
        super(itemView);
        final View dialogBayar = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_bayar, null);
        final Context contx = ctx;

        txtNama = itemView.findViewById(R.id.txtNamaRoti);
        txtQty = itemView.findViewById(R.id.txtQty);
        txtTotal = itemView.findViewById(R.id.txtTotal);
        llBawah = itemView.findViewById(R.id.llBawah);
        btnCicil = itemView.findViewById(R.id.btnCicil);

        db_edtBayar = dialogBayar.findViewById(R.id.db_edtBayar);
        db_edtKembalian = dialogBayar.findViewById(R.id.db_edtKembalian);
        db_edtKeterangan = dialogBayar.findViewById(R.id.db_edtKeterangan);

        databaseReference = MainActivity.getDatabase().getReference();
        databaseReference.keepSynced(true);

        //setup dialog bayar
        final AlertDialog.Builder dialogBuilderBayar = new AlertDialog.Builder(ctx);
        dialogBuilderBayar.setView(dialogBayar);
        dialogBuilderBayar.setTitle("Bayar cicilan");
        dialogBuilderBayar.setMessage("silahkan isi sesuai pesanan");
        dialogBuilderBayar.setPositiveButton("Bayar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //transaksiItems.add(new TransaksiItem("syidik", ));
                totalBayar = totalBayar + Integer.parseInt(db_edtBayar.getText().toString());
                if (String.valueOf(totalBayar) == getTotal) {
                    databaseReference.child("transaksi/"+key+"/status").setValue("selesai");
                }
                databaseReference.child("transaksi/"+key+"/bayar").setValue(String.valueOf(totalBayar));
                Toast.makeText(contx, "Berhasil ditambahkan "+totalBayar, Toast.LENGTH_SHORT).show();
            }
        });
        dialogBuilderBayar.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        db_edtBayar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() != 0) {
                    kembalian = Integer.parseInt(getTotal) - totalBayar;
                    kembalian = Integer.parseInt(s.toString()) - kembalian;

                    db_edtKembalian.setText(String.valueOf(kembalian));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        db_edtKembalian.setEnabled(false);
        db = dialogBuilderBayar.create();
        //end setting dialog bayar

        btnCicil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseReference.child("transaksi").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        totalBayar = Integer.parseInt(dataSnapshot.child(key).child("bayar").getValue().toString());
                        getTotal = dataSnapshot.child(key).child("total").getValue().toString();
                        //transaksi = dataSnapshot.child(key).getValue(PushTransaksi.class);
                        //totalBayar = Integer.parseInt(transaksi.getBayar());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                db.show();
            }
        });
    }

    public void setTxtNama(String nama) {
        txtNama.setText(nama);
    }

    public void setShow(String key) {
        this.key = key;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llBawah.setVisibility(View.VISIBLE);
        llBawah.setLayoutParams(params);
    }

    public void setHide(String key) {
        this.key = key;
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) llBawah.getLayoutParams();
        params.height = 0;
        llBawah.setVisibility(View.INVISIBLE);
        llBawah.setLayoutParams(params);
    }

    public void setTxtQty(String qty) {
        txtQty.setText(qty);
    }

    public void setTxtTotal(String total) {
        txtTotal.setText(total);
    }
}
