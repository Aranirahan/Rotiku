package com.aranirahan.rotiku.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.aranirahan.rotiku.fragment.TabFragmentBooking;
import com.aranirahan.rotiku.fragment.TabFragmentDone;

public class HistoryAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public HistoryAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                TabFragmentBooking tab1 = new TabFragmentBooking();
                return tab1;
            case 1:
                TabFragmentDone tab2 = new TabFragmentDone();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
