package com.aranirahan.rotiku.adapter;

import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.aranirahan.rotiku.R;
import com.aranirahan.rotiku.model.TransaksiItem;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class TitleViewHolder extends GroupViewHolder {
    private TextView txtNama, txtTglAmbil, txtTotal;
    private ImageView imgArrow;

    public TitleViewHolder(View itemView) {
        super(itemView);
        txtNama = itemView.findViewById(R.id.nama_pesan);
        txtTglAmbil = itemView.findViewById(R.id.tgl_ambil);
        txtTotal = itemView.findViewById(R.id.total);
        imgArrow = itemView.findViewById(R.id.imgArrow);
    }

    public void setTitle(ExpandableGroup group) {
        txtNama.setText(((TransaksiItem) group).getNama_pesan());
        txtTglAmbil.setText(((TransaksiItem) group).getTotal());
        txtTotal.setText(((TransaksiItem) group).getTgl_ambil());
    }

    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        imgArrow.setAnimation(rotate);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        imgArrow.setAnimation(rotate);
    }
}
