package com.aranirahan.rotiku.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aranirahan.rotiku.R;
import com.aranirahan.rotiku.model.SubTransaksiItem;
import com.aranirahan.rotiku.model.TransaksiItem;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class BookingAdapter extends ExpandableRecyclerViewAdapter<TitleViewHolder, SubTitleViewHolder> {
    private List<TransaksiItem> transaksiItems;
    private Context context;

    public BookingAdapter(List <TransaksiItem> groups, Context ctx) {
        super(groups);
        this.context = ctx;

    }

    @Override
    public TitleViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking, parent, false);
//        ViewHolder vh = new ViewHolder(view);
        return new TitleViewHolder(view);
    }

    @Override
    public SubTitleViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking_sub, parent, false);
//        ViewHolder vh = new ViewHolder(view);
        return new SubTitleViewHolder(view, context, parent);
    }

    @Override
    public void onBindChildViewHolder(SubTitleViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final SubTransaksiItem subTitle = ((TransaksiItem) group).getItems().get(childIndex);
        Log.d("cek", "cekker "+((TransaksiItem) group).getKaryawan());
        holder.setTxtNama(subTitle.getProduk());
        holder.setTxtQty(subTitle.getQty());
        holder.setTxtTotal(subTitle.getTotal());

        if (((TransaksiItem) group).getItemCount() == (childIndex+1)) {
            holder.setShow(((TransaksiItem) group).getKaryawan());
        } else {
            holder.setHide(((TransaksiItem) group).getKaryawan());
        }
//        holder.llBawah.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onBindGroupViewHolder(TitleViewHolder holder, int position, ExpandableGroup group) {

        holder.setTitle(group);
//        final Title title = ((Title) group.getItems().get(position));
//        holder.setNama(title.getTxtNamaPemesan());
//        holder.setTglAmbil(title.getTxtTglAmbil());
    }

    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }


//    public class ViewHolder extends RecyclerView.ViewHolder {
//
//        TextView tvNamaPesan, tvTotal, tvTglAmbil;
//        public ViewHolder(View itemView) {
//            super(itemView);
//            tvNamaPesan = itemView.findViewById(R.id.nama_pesan);
//            tvTotal = itemView.findViewById(R.id.total);
//            tvTglAmbil = itemView.findViewById(R.id.tgl_ambil);
//        }
//    }
}