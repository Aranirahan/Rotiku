package com.aranirahan.rotiku.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.aranirahan.rotiku.R;
import com.aranirahan.rotiku.fragment.DashboardFragment;
import com.aranirahan.rotiku.fragment.HistoryFragment;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton rb_dash, rb_history;
    private static FirebaseDatabase mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        radioGroup = findViewById(R.id.rg_menu);
        rb_dash = findViewById(R.id.rb_dashboard);
        rb_history = findViewById(R.id.rb_history);

        initFragment(new DashboardFragment());

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_dashboard:
                        initFragment(new DashboardFragment());

                        break;
                    case R.id.rb_history:
                        initFragment(new HistoryFragment());
//                        setTint(drawDashboard, Color.rgb(96, 125, 139));
//                        setTint(drawHistory, Color.rgb(124, 252, 0));

                        break;
                }
            }
        });
//        public static Drawable setVectorForPreLollipop(int resourceId, Context activity) {
//            Drawable icon;
//            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//                icon = VectorDrawableCompat.create(this.getResources(), resourceId, this.getTheme());
//            } else {
//                icon = this.getResources().getDrawable(resourceId, this.getTheme());
//            }
//
//            return icon;
//        }

        //initFragment(new DashboardFragment());
//        setTint(drawHistory, Color.rgb(96, 125, 139));
//        setTint(drawDashboard, Color.rgb(124, 252, 0));
//        FragmentManager mFragmentManager = getSupportFragmentManager();
//        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
//        DashboardFragment mHomeFragment = new DashboardFragment();
//        mFragmentTransaction.add(R.id.dashboard_main, mHomeFragment, DashboardFragment.class.getSimpleName());
//        Log.d("MyDashboardMain", "Fragment Name :" + DashboardFragment.class.getSimpleName());
//        mFragmentTransaction.commit();
    }

    public static FirebaseDatabase getDatabase() {
        if (mDatabase == null) {
           mDatabase = FirebaseDatabase.getInstance();
           mDatabase.setPersistenceEnabled(true);
        }
        return mDatabase;
    }

//    public static Drawable setTint(Drawable drawable, int color) {
//        final Drawable newDrawable = DrawableCompat.wrap(drawable);
//        DrawableCompat.setTint(newDrawable, color);
//        return newDrawable;
//    }

    private void initFragment(Fragment classFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.dashboard_main, classFragment);
        transaction.commit();
    }
}
