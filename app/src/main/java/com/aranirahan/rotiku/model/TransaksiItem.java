package com.aranirahan.rotiku.model;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class TransaksiItem extends ExpandableGroup<SubTransaksiItem> {
    private String key;
    private String karyawan;
    private String nama_pesan;
    private String status;
    private String tgl_ambil;
    private String tgl_transaksi;
    private String total;
    public int produks;

    public TransaksiItem(String karyawan, String nama_pesan, String status, String tgl_ambil, String tgl_transaksi, String total, List<SubTransaksiItem> produks) {
        super(karyawan, produks);
        this.karyawan = karyawan;
        this.nama_pesan = nama_pesan;
        this.status = status;
        this.tgl_ambil = tgl_ambil;
        this.tgl_transaksi = tgl_transaksi;
        this.total = total;
        this.produks = produks.size();
    }

    public int getProduks() {
        return produks;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKaryawan() {
        return karyawan;
    }

    public void setKaryawan(String karyawan) {
        this.karyawan = karyawan;
    }

    public String getNama_pesan() {
        return nama_pesan;
    }

    public void setNama_pesan(String nama_pesan) {
        this.nama_pesan = nama_pesan;
    }

    public String getTgl_transaksi() {
        return tgl_transaksi;
    }

    public void setTgl_transaksi(String tgl_transaksi) {
        this.tgl_transaksi = tgl_transaksi;
    }

    public String getTgl_ambil() {
        return tgl_ambil;
    }

    public void setTgl_ambil(String tgl_ambil) {
        this.tgl_ambil = tgl_ambil;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

}
