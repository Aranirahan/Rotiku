package com.aranirahan.rotiku.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SubTransaksiItem implements Parcelable {
    String key;
    String produk;
    String qty;
    String total;

    public SubTransaksiItem() {   }

    protected SubTransaksiItem(Parcel in) {
        this.produk = in.readString();
    }

    public SubTransaksiItem(String produk, String qty, String total) {
        this.produk = produk;
        this.qty = qty;
        this.total = total;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getProduk() {
        return produk;
    }

    public void setProduk(String produk) {
        this.produk = produk;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(produk);
    }

    public static final Creator<SubTransaksiItem> CREATOR = new Creator<SubTransaksiItem>() {
        @Override
        public SubTransaksiItem createFromParcel(Parcel in) {
            return new SubTransaksiItem(in);
        }

        @Override
        public SubTransaksiItem[] newArray(int size) {
            return new SubTransaksiItem[size];
        }
    };
}
