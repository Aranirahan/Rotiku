package com.aranirahan.rotiku.model;

public class RotiItem {
    private String key;
    private String nama;
    private String gambar;
    private String harga;
    private String stock;


    public RotiItem(String nama, String gambar, String harga, String stock) {
        this.nama = nama;
        this.gambar = gambar;
        this.harga = harga;
        this.stock = stock;
    }

    public RotiItem() {

    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }
}
