package com.aranirahan.rotiku.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataSql extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "tokokue,db";
    private static final int DATABASE_VERSION = 1;

    public DataSql(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE transaksi(id integer primary key autoincrement not null, nama_pesan text null, karyawan text null, " +
                "status text null, tgl_ambil text null, tgl_transaksi text null, total integer null);";
        db.execSQL(sql);
        sql = "CREATE TABLE sub_transaksi (id integer primary key autoincrement not null, id_transaksi integer null," +
                "id_produk integer null, qty integer null, total integer null);";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void insert_transaksi(String nama_pesan, String karyawan, String status, String tgl_ambil, String tgl_transaksi, int total) {
        ContentValues contentValues = new ContentValues();
//        contentValues.put("nama_pesan", nama_pesan);
//        contentValues.put("nama_pesan", nama_pesan);
//        contentValues.put("nama_pesan", nama_pesan);
    }
}
